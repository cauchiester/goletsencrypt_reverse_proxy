package main

import (
	"fmt"
	"net/http"
)
//CGO_ENABLED=0 GOOS=linux go build  -ldflags="-w -s" -o ./docker/output main.go
func main() {
	http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Welcome to my website!")
	})

	/*fs := http.FileServer(http.Dir("static/"))
	http.Handle("/", http.StripPrefix("/", fs))
	*/
	http.ListenAndServe(":4444", nil)
}
