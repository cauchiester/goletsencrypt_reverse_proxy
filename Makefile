

all: build

dep: 


build:  ## Build the binary file
  CGO_ENABLED=0 GOOS=linux @go build  -ldflags="-w -s" -o ./docker/output main.go


clean:
  rm -f ./docker/output