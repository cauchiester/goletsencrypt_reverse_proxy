package main

// https://blog.kowalczyk.info/article/Jl3G/https-for-free-in-go.html
// To run:
// go run main.go
// Command-line options:
//   -production : enables HTTPS on port 443
//   -redirect-to-https : redirect HTTP to HTTTPS

import (
	"context"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
    "net/http/httputil"
    "net/url"
	"time"
	"os"
	"golang.org/x/crypto/acme/autocert"

)

const (
	htmlIndex = `<html><body>Welcome in openid.trustus.fr</body></html>`
	httpPort  = ":80"
)

var (
	flgProduction          = true
	flgRedirectHTTPToHTTPS = true
	//f, _ = os.OpenFile("../logfile", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	config = Config{}
)

func handleIndex(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, htmlIndex)
}

func makeServerFromMux(mux *http.ServeMux) *http.Server {
	// set timeouts so that a slow or malicious client doesn't
	// hold resources forever
	return &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      mux,
	}
}

func makeHTTPServer() *http.Server {
	mux := &http.ServeMux{}
	//mux.HandleFunc("/", handleIndex)
	//connectHandler(mux);
	u, _ := url.Parse(config.Redirect)
    mux.Handle("/", httputil.NewSingleHostReverseProxy(u))
	//mux.Handle("/", http.FileServer(http.Dir("./static")))
	return makeServerFromMux(mux)

}

func makeHTTPToHTTPSRedirectServer() *http.Server {
	handleRedirect := func(w http.ResponseWriter, r *http.Request) {
		newURI := "https://" + r.Host + r.URL.String()
		http.Redirect(w, r, newURI, http.StatusFound)
	}
	mux := &http.ServeMux{}
	mux.HandleFunc("/", handleRedirect)
	return makeServerFromMux(mux)
}

func parseFlags() {
	flag.BoolVar(&flgProduction, "production", false, "if true, we start HTTPS server")
	flag.BoolVar(&flgRedirectHTTPToHTTPS, "redirect-to-https", false, "if true, we redirect HTTP to HTTPS")
	flag.Parse()
}
func configureLog(){
	/*f, err := os.OpenFile("../testlogfile", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
	    log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()*/

	//log.SetOutput(f)
	//log.Println("This is a test log entry")
}

type Config struct{
	Host string
	Redirect string
}
func main() {
	//parseFlags()
	configureLog()
	log.Println("Reading config");
/*

	file, err := ioutil.ReadFile("config.json")
	if err != nil {
		log.Println(" |- Openning failed")
		return
	}

	err = json.Unmarshal(file, &config)
	if err != nil {
		log.Println(" |- Unmarshal failed")
		return
	}
	log.Println(" |- Ok")
*/
	config.Host = os.Getenv("HOST")
	config.Redirect = os.Getenv("REDIRECT")

	//config.Host = "runners.1.trustus.fr"
	//config.Redirect = "https://www.google.fr"

	log.Println("Configuration = ")
	log.Println(" |- HOST     = ", config.Host)
	log.Println(" |- Redirect = ", config.Redirect)


	log.Println("Starting main entrypoint");
	var m *autocert.Manager

	var httpsSrv *http.Server
	if flgProduction {
		hostPolicy := func(ctx context.Context, host string) error {
			// Note: change to your real host
			allowedHost := config.Host
			if host == allowedHost {
				return nil
			}
			return fmt.Errorf("acme/autocert: only %s host is allowed", allowedHost)
		}

		dataDir := "."
		m = &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: hostPolicy,
			Cache:      autocert.DirCache(dataDir),
		}

		httpsSrv = makeHTTPServer()
		httpsSrv.Addr = ":443"
		httpsSrv.TLSConfig = &tls.Config{GetCertificate: m.GetCertificate}

		go func() {
			fmt.Printf("Starting HTTPS server on %s\n", httpsSrv.Addr)
			err := httpsSrv.ListenAndServeTLS("", "")
			if err != nil {
				log.Fatalf("httpsSrv.ListendAndServeTLS() failed with %s", err)
			}
		}()
	}

	var httpSrv *http.Server
	if flgRedirectHTTPToHTTPS {
		httpSrv = makeHTTPToHTTPSRedirectServer()
	} else {
		httpSrv = makeHTTPServer()
	}
	// allow autocert handle Let's Encrypt callbacks over http
	if m != nil {
		httpSrv.Handler = m.HTTPHandler(httpSrv.Handler)
	}

	httpSrv.Addr = httpPort
	fmt.Printf("Starting HTTP server on %s\n", httpPort)

	err := httpSrv.ListenAndServe()
	if err != nil {
		log.Fatalf("httpSrv.ListenAndServe() failed with %s", err)
	}
	
}
/*
func main() {
	config.Host = os.Getenv("HOST")
	config.Redirect = os.Getenv("REDIRECT")

	//config.Host = "runners.1.trustus.fr"
	//config.Redirect = "https://www.google.fr"

	log.Println("Configuration = ")
	log.Println(" |- HOST     = ", config.Host)
	log.Println(" |- Redirect = ", config.Redirect)


	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello world"))
	})

	certManager := autocert.Manager{
		Prompt:     autocert.AcceptTOS,
		Cache:      autocert.DirCache("cert-cache"),
		// Put your domain here:
		HostPolicy: autocert.HostWhitelist("kappa.serv.brendanr.net"),
	}

	server := &http.Server{
		Addr:    ":443",
		Handler: mux,
		TLSConfig: &tls.Config{
			GetCertificate: certManager.GetCertificate,
		},
	}

	go http.ListenAndServe(":80", certManager.HTTPHandler(nil))
	server.ListenAndServeTLS("", "")
}*/